# 爬取免费代理IP

#### 项目介绍
使用爬虫技术爬取免费代理IP网站提供的代理IP,并测试筛选出能够使用的IP.

***在爬取一些大网站的时候,总会出现被反爬技术阻碍的情况,限制IP就是其中一种,那么使用代理就是很好的解决方案.
本次爬取的是西刺代理,虽然西刺提供了接口,但是使用并不好用,经常无响应,接口不能用,就只能自己动手啦
为了给一些小白提供些便利,就把代码贴上来供大家参考

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)