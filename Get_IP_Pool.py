import requests
import random
import re
import json
import time

#随机返回一个UserAgent,应对西刺的反爬
def rand_userAgent():
	#我自己的UA池,大家也可以自己动手做一个,千万不要使用CV大法,程序员真的丢不起这个人
	UserAgentList = [{'User-Agent': 'Mozilla/5.0(Macintosh;U;IntelMacOSX10_6_8;en-us)AppleWebKit/534.50(KHTML,likeGecko)Version/5.1Safari/534.50'}, {'User-Agent': 'Mozilla/5.0(Windows;U;WindowsNT6.1;en-us)AppleWebKit/534.50(KHTML,likeGecko)Version/5.1Safari/534.50'}, {'User-Agent': 'Mozilla/5.0(compatible;MSIE9.0;WindowsNT6.1;Trident/5.0;'}, {'User-Agent': 'Mozilla/4.0(compatible;MSIE8.0;WindowsNT6.0;Trident/4.0)'}, {'User-Agent': 'Mozilla/4.0(compatible;MSIE7.0;WindowsNT6.0)'}, {'User-Agent': 'Mozilla/4.0(compatible;MSIE6.0;WindowsNT5.1)'}, {'User-Agent': 'Mozilla/5.0(Macintosh;IntelMacOSX10.6;rv:2.0.1)Gecko/20100101Firefox/4.0.1'}, {'User-Agent': 'Mozilla/5.0(WindowsNT6.1;rv:2.0.1)Gecko/20100101Firefox/4.0.1'}, {'User-Agent': 'Opera/9.80(Macintosh;IntelMacOSX10.6.8;U;en)Presto/2.8.131Version/11.11'}, {'User-Agent': 'Opera/9.80(WindowsNT6.1;U;en)Presto/2.8.131Version/11.11'}, {'User-Agent': 'Mozilla/5.0(Macintosh;IntelMacOSX10_7_0)AppleWebKit/535.11(KHTML,likeGecko)Chrome/17.0.963.56Safari/535.11'}, {'User-Agent': 'Mozilla/4.0(compatible;MSIE7.0;WindowsNT5.1;Maxthon2.0)'}, {'User-Agent': 'Mozilla/4.0(compatible;MSIE7.0;WindowsNT5.1;TencentTraveler4.0)'}, {'User-Agent': 'Mozilla/4.0(compatible;MSIE7.0;WindowsNT5.1)'}, {'User-Agent': 'Mozilla/4.0(compatible;MSIE7.0;WindowsNT5.1;TheWorld)'}, {'User-Agent': 'Mozilla/4.0(compatible;MSIE7.0;WindowsNT5.1;Trident/4.0;SE2.XMetaSr1.0;SE2.XMetaSr1.0;.NETCLR2.0.50727;SE2.XMetaSr1.0)'}, {'User-Agent': 'Mozilla/4.0(compatible;MSIE7.0;WindowsNT5.1;360SE)'}, {'User-Agent': 'Mozilla/4.0(compatible;MSIE7.0;WindowsNT5.1;AvantBrowser)'}, {'User-Agent': 'Mozilla/4.0(compatible;MSIE7.0;WindowsNT5.1)'}]
	return random.choices(UserAgentList)[0]

#向一个url发起请求,成功返回文本,不成功继续发起请求
def get_one_page(url):
	print("发起请求")
	userAgent = rand_userAgent()
	response = requests.get(url,headers=userAgent)
	response.encoding = "utf-8"
	if response.status_code == 200:
		# print(response.text)
		print("响应成功")
		return response.text
	else:
		print("响应失败")
		#如果一直响应失败会超出递归最大层数,导致程序崩溃.
		#此处并不打算来解决这个问题,毕竟玩具代码,不影响实用
		return get_one_page(url)


"""所需信息所在板块的html原样
<tr class="odd">
    <td class="country"><img src="http://fs.xicidaili.com/images/flag/cn.png" alt="Cn" /></td>
    <td>175.148.71.249</td>
    <td>1133</td>
    <td>辽宁葫芦岛</td>
    <td class="country">高匿</td>
    <td>HTTP</td>
      <td>59天</td>
    <td>2分钟前</td>
  </tr>
  
  <tr class="">
    <td class="country"><img src="http://fs.xicidaili.com/images/flag/cn.png" alt="Cn" /></td>
    <td>180.118.240.155</td>
    <td>808</td>
    <td>江苏镇江</td>
    <td class="country">高匿</td>
    <td>HTTPS</td>
      <td>467天</td>
    <td>3分钟前</td>
  </tr>
"""

#使用正则格式化处理文本
def extract_page(text):
	str1 = '<td class="country"><img[\s\S]+?<td>'
	#ip
	str2 = '</td>[\s\S]*?<td>'
	#端口号
	str3 = '</td>[\s\S]*?<td>[\s\S]*?<td>'
	#协议类型
	str4 = '</td>'
	IP_List = re.findall(str1+"([\s\S]*?)"+str2+"([\s\S]*?)"+str3+"([\s\S]*?)"+str4, text)
	return IP_List

#获取可用的IP_list
def ip_pool(IP_list):
	for i in IP_list:
		#如果IP不可用,从IP_list中删除
		if not test_ip(i[0],i[1]):
			print(i,"不可用")
			IP_list.remove(i)
	#返回筛选后的IP_list
	return IP_list

#对每一个IP进行测试
def test_ip(ip,port):
	server = ip+":"+port
	proxies = {'http': 'http://' + server, 'https': 'https://' + server}
	try:
		r = requests.get('https://www.baidu.com/', proxies=proxies,timeout=1)
		if r.status_code == 200:
			return 1
		else:
			return 0
	except:
		return 0 


def main():
	IP_text = get_one_page("http://www.xicidaili.com/")
	IP_list = extract_page(IP_text)
	IP_pool = ip_pool(IP_list)
	#将IP写成json格式,写入文件,覆盖原有文件
	IP_json = json.dump(IP_pool,open("ip-15min_fresh.txt",'wt'))

if __name__ == '__main__':
	#每隔16分钟刷新一次
	while 1:
		main()
		print(time.ctime(),"刷新成功")
		time.sleep(60*16)

